package hr.tvz.laktasic;

import hr.tvz.laktasic.domain.Notebook;
import hr.tvz.laktasic.domain.User;

import java.util.ArrayList;
import java.util.List;


public class MockHelper {
    public static List<User> mockUserList() {
        List<User> users = new ArrayList<>();
        users.add(new User((long) 1,"pperic","pero","peric"));
        users.add(new User((long) 2,"iivic","ivo","ivic"));
        users.add(new User((long) 3,"ddragic","drago","dragic"));
        return users;
    }

    public static List<Notebook> mockNotebookList(){
        List<Notebook> notebooks = new ArrayList<>();
        notebooks.add(new Notebook(1,"prvaBiljeznica","Ovo je prva biljeznica"));
        notebooks.add(new Notebook(2,"drugaBiljeznica","Ovo je druga biljeznica"));
        notebooks.add(new Notebook(3,"trecaBiljeznica","Ovo je treca biljeznica"));
        notebooks.add(new Notebook(4,"cetvrtaBiljeznica","Ovo je cetvrta biljeznica"));
        notebooks.add(new Notebook(5,"petaBiljeznica","Ovo je peta biljeznica"));
        return  notebooks;
    }
}
