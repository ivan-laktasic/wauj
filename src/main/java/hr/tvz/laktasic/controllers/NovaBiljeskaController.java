package hr.tvz.laktasic.controllers;

import hr.tvz.laktasic.MockHelper;
import hr.tvz.laktasic.domain.Note;
import hr.tvz.laktasic.domain.Notebook;
import hr.tvz.laktasic.domain.User;
import hr.tvz.laktasic.models.NovaBiljeskaForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by ivanl on 19-Mar-17.
 */
@Controller
public class NovaBiljeskaController {
    @RequestMapping("/novaBiljeska")
    public String biljeskaInit(Model model){
        model.addAttribute("userList",MockHelper.mockUserList());
        model.addAttribute("notebookList",MockHelper.mockNotebookList());
        model.addAttribute("novaBiljeskaForm",new NovaBiljeskaForm());
        return "novaBiljeska";
    }

    @RequestMapping(path = "/novaBiljeska", method = RequestMethod.POST)
    public String novaBiljeska(@ModelAttribute NovaBiljeskaForm novaBiljeskaForm, Model model){
        List<User> users=MockHelper.mockUserList();
        List<Notebook> notebooks=MockHelper.mockNotebookList();
        User user;
        Notebook notebook;
        if(novaBiljeskaForm.getUserID().equals("null")){
            user=null;
        }else
        {
            user=users.stream().filter(x-> x.getId().equals(Long .parseLong(novaBiljeskaForm.getUserID()))).findFirst().get();
        }

        if(novaBiljeskaForm.getNotebookID().equals("null")){
            notebook=null;
        }else
        {
            notebook=notebooks.stream().filter(x->x.getId().equals(Long .parseLong(novaBiljeskaForm.getNotebookID()))).findFirst().get();
        }
        Note note= new Note(user, notebook, novaBiljeskaForm.getTitle(), novaBiljeskaForm.getText());
        model.addAttribute("note",note);
        return "biljeska";

    }
}
