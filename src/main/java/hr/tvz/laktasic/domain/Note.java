package hr.tvz.laktasic.domain;

public class Note {
    private User user;
    private Notebook notebook;
    private String title;
    private String text;

    public Note(User user, Notebook notebook, String title, String text) {
        this.user = user;
        this.notebook = notebook;
        this.title = title;
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Notebook getNotebook() {
        return notebook;
    }

    public void setNotebook(Notebook notebook) {
        this.notebook = notebook;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
