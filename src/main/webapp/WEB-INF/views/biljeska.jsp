<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" %>
<html>
<head>
    <title>Biljeska</title>
    <link href="<spring:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <script src="<spring:url value="/resources/js/bootstrap.js" />"></script>
</head>
<body>
    <body class="container-fluid">
        <div class="row text-center">
            <div class="col-md-4 col-md-offset-4">
                <div class="row">
                    <div class="col-lg-6 well">
                        <c:choose>
                            <c:when test="${not empty note.user}">
                            <h4>Autor: ${note.user.fullName}</h4>
                            <p>Korisnicko ime: ${note.user.userName}</p>
                            </c:when>
                            <c:otherwise>
                                <h4>Niste unijeli korisnika</h4>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-lg-6 well">
                        <c:choose>
                            <c:when test="${not empty note.notebook}">
                                <h4>Biljeznica: ${note.notebook.name}</h4>
                                <p>Opis: ${note.notebook.description}</p>
                            </c:when>
                            <c:otherwise>
                                <h4>Niste unijeli biljesku</h4>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="row jumbotron">
                     <c:choose>
                        <c:when test="${not empty note.title}">
                            <h1>${note.title}</h1>
                        </c:when>
                        <c:otherwise>
                            <h4>Niste unijeli naslov</h4>
                        </c:otherwise>
                     </c:choose>

                    <br/>
                     <c:choose>
                        <c:when test="${not empty note.text}">
                            <p>${note.text}</p
                        </c:when>
                        <c:otherwise>
                            <h4>Niste unijeli tekst</h4>
                        </c:otherwise>
                     </c:choose>
                    <br/>
                    <a href="<spring:url value="/novaBiljeska" />" >Povratak</a>
                </div>
            </div>
        </div>
    </body>
</html>
