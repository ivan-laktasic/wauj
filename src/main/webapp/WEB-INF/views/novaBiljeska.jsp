<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Nova Biljeska</title>
    <link href="<spring:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <script src="<spring:url value="/resources/js/bootstrap.js" />"></script>
</head>
    <body class="container-fluid">
        <div class="row text-center">
            <div class="col-md-4 col-md-offset-4">
                <h1 class=>Nova biljeska</h1>
                <form:form class="form-group" method="post" modelAttribute="novaBiljeskaForm">
                    <form:select class="form-control" id="userID" path="userID">
                        <option value="null">Odaberite korisnika</option>
                        <form:options items="${userList}" itemLabel="fullName" itemValue="id"/>
                    </form:select>
                    <br/>
                    <form:select class="form-control" id="notebookID" path="notebookID">
                        <option value="null">Odaberite biljeznicu</option>
                        <form:options items="${notebookList}" itemLabel="name" itemValue="id"/>
                    </form:select>
                    <br/>
                    <label for="title">Naslov</label>
                    <form:input type="text" cssClass="form-control" path="title" id="title"/>
                    <br/>
                    <label for="text">Tekst</label>
                    <form:textarea path="text" cssClass="form-control" id="text"></form:textarea>
                    <br/>
                    <form:button type="submit" class="btn btn-success">Posalji</form:button>
                </form:form>
            </div>
        </div>
    </body>
</html>
